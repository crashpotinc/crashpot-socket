
const 
http = require("http"),
express = require("express"),
app = express(),
socketio = require("socket.io"),
mysql = require('mysql'),
dateFormat = require('dateformat'),
admin = require("firebase-admin"),
serviceAccount = require("./crashpot-firebase-adminsdk.json");
const sleep = require('util').promisify(setTimeout);
var errorLog = require('./logger');


/* 
   = display public webpage on run
   ---------------------------------------------------- */
app.use(express.static("public"));

/* 
   = mysql database connection
   ---------------------------------------------------- */
var db_config = {
    connectionLimit : 10,
    host: 'localhost',
    user: 'root',
    password: 'H72#^Fub{24.V;',
    database: 'crashpot',
    charset: 'utf8mb4'
};



var pool  = mysql.createPool(db_config);


// push notification configuration
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://crashpot-dc60a.firebaseio.com"
});

// create http server and wrap the express app
const server = http.createServer(app);

// bind socket.io to that server
const io = socketio(server);

// will fire for every new websocket connection
io.on("connection", (socket) => {
    console.info(`Socket ${socket.id} has connected.`);
    socket.on("disconnect", () => {
        console.info(`Socket ${socket.id} has disconnected.`);
    });
    //receive chat message and send it back to user
    socket.on("newMessage", (data) => {
        let object = JSON.parse(data);
        let now = new Date();
        let dbObject = {
            user_id: object.userId,
            message: object.message,
            messageType: parseInt(object.messageType),
            created_at: dateFormat(now, "yyyy-mm-dd HH:MM:ss")
        };
        if(object.tagUserList) {
            dbObject.tagUserList = object.tagUserList;
        }
        pool.getConnection(function(err, connection) {
            if (err) throw err; // not connected!
            connection.query('INSERT INTO chat_logs SET ?', dbObject, function(err, result) {
                if (err) {
                    errorLog.error("error while adding to chat logs", JSON.stringify(err));
                } else {
                    connection.query('UPDATE `users` SET  ? WHERE id = ' + object.userId , { last_read_id: result.insertId }, function(err, updateRow) {
                        connection.release();
                        if (err) {
                            errorLog.error("error while updating user in chat", JSON.stringify(err));
                        }
                        object.messageId = result.insertId;
                        object.time = dbObject.created_at;
                        if(parseInt(object.messageType) === 2) {
                            io.sockets.emit("newMessage", object);
                        } else {
                            io.emit("newMessage", object);
                        }
                        if(object.pushId) {
                            sendPushNotification(object);
                        }
                    });
                }
            });
        });
    });

    // handle user hit button event while game running
    socket.on("betPoint", (data) => {
        data = JSON.parse(data);
        console.log("data", data);
        let user_bats = {
            user_id: data.userId,
            user_bet_game_id: data.gameId,
            bet_at: data.xAxis,
            y_axis: data.yAxis,
            bet_amount: data.betAmount
        };
        if(data.status !== 2 && data.status !== 3) {
            if(data.xAxis && data.finalValue) {
                if(data.xAxis < data.finalValue) {
                    user_bats.status = 1;
                    user_bats.calculate_amount = (data.betAmount * data.yAxis).toFixed(2);
                    user_bats.calculate_xp = ((100*data.yAxis)/data.rankingByLevel);
                } else {
                    user_bats.status = 0;
                }
            } else {
                user_bats.status = 2;
            }
            updateUserInformations(data, user_bats, true, false);
        } else {
            user_bats.status = data.status;
        }

        if(data.betId && data.betId !== 0) {
            let id = data.betId;
            delete data.betId;
            pool.getConnection(function(err, connection) {
                if (err) throw err; // not connected!
                connection.query('UPDATE `user_bet_results` SET  ? WHERE id = ' + id, user_bats, function(err, result) {
                    connection.release();
                    if (err) {
                        errorLog.error("error while updating user bet result in game", JSON.stringify(err));
                    } 
                    else { 
                        data.betId = id;         
                        io.emit("crashStart", data);
                    }
                });
            });
        } else {
            pool.getConnection(function(err, connection) {
                if (err) throw err;
                connection.query('INSERT INTO user_bet_results SET ?', user_bats, function(err, result) {
                    connection.release();
                    if (err) {
                        errorLog.error("error while adding user bet result in game", JSON.stringify(err));
                    } else { 
                        data.betId = result.insertId;        
                        io.emit("crashStart", data);
                    }
                });
            });
        }
    });
});

// important! must listen from `server`, not `app`, otherwise socket.io won't function correctly
const SERVER_PORT = 3002;
server.listen(SERVER_PORT, () => {
    console.info(`Listening on port ${SERVER_PORT}.`);
    let passRandomNumber = generateRandomNumber();
    let gameObject = {
        name: "game at " +Date.now(),
        length: passRandomNumber
    };
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query('INSERT INTO user_bet_games SET ?', gameObject, function(err, result) {
            connection.release();
            if (err) {
                errorLog.error("error while adding user bet game", JSON.stringify(err));
            } else {
                displayTimer(io, passRandomNumber, result.insertId);
            }
        }); 
    });
}); 


/* 
   = update users total amount and emit result
   ---------------------------------------------------- */
function updateUserInformations(data, user_bats, is_emit, is_loss) {
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query("Select totalCoins, profit, wagered, totalXP, rankingByLevel, playedGames from users where id = " + data.userId, function(err, result) {
            if(err) {
                errorLog.error("error while fetching user detail", JSON.stringify(err));
            } else {
                errorLog.info("user " + JSON.stringify(result[0]));
                errorLog.info("user bets" + JSON.stringify(user_bats));
                if(user_bats.calculate_amount) {
                    user_bats.total_amount = parseFloat(result[0].totalCoins) + (parseFloat(user_bats.calculate_amount) - parseFloat(user_bats.bet_amount));
                    user_bats.profit = parseFloat(result[0].profit) + (parseFloat(user_bats.calculate_amount) - parseFloat(user_bats.bet_amount));
                } else {
                    user_bats.total_amount = parseFloat(result[0].totalCoins) - parseFloat(user_bats.bet_amount);
                    user_bats.profit = parseFloat(result[0].profit) - parseFloat(user_bats.bet_amount);
                }
                errorLog.info("user_bats.y_axis" + user_bats.y_axis);
                errorLog.info("result[0].rankingByLevel" + result[0].rankingByLevel);
                let calculated_xp = ((100*user_bats.y_axis)/result[0].rankingByLevel);
                errorLog.info("calculated_xp " + calculated_xp);
                let xpAwarded = result[0].totalXP + calculated_xp;
                errorLog.info("xpAwarded " + xpAwarded);
                let rankingByLevel = parseInt(xpAwarded / 1000);
                errorLog.info("rankingByLevel " + rankingByLevel);
                let playedGames = result[0].playedGames + 1;
                user_bats.calculated_xp = calculated_xp;

                user_bats.wagered = parseFloat(result[0].wagered) + parseFloat(user_bats.bet_amount);
                let updateUserObject = {
                    totalCoins: user_bats.total_amount, 
                    profit: user_bats.profit, 
                    playedGames: playedGames,
                    rankingByLevel: rankingByLevel,
                    wagered: user_bats.wagered
                };
                if(!is_loss) {
                    updateUserObject.totalXP = xpAwarded;
                }
                connection.query('UPDATE `users` SET  ? WHERE id = ' + data.userId, updateUserObject, function(err, result) {
                    connection.release();
                    if (err) {
                        errorLog.error("error while updating user in game", err);
                        errorLog.info("user id" + data.userId + "::" + JSON.stringify(updateUserObject))
                    }
                    if(is_emit) {
                        io.emit("betPointResult", user_bats);
                    }
                });
            }
        });
    });
}

/* 
   = generate random number
   ---------------------------------------------------- */
function generateRandomNumber() {
    var zmii = 10.0;
    var isOne, isOneToTwo, isTwoToFive, isFiveToTen, isTenToFifty, isFiftyToHundred;
    var scale = 1;
    bet = (51 - (Math.log((Math.random()*(Math.pow(zmii, 50)-1.0))+1.0) / Math.log(zmii))).toFixed(2);
    
    isOneToTwo = Math.floor(Math.random() * (100));
    if(isOneToTwo < 90){
        isOne = Math.floor(Math.random() * (100));
        if(isOne < 90){
            scale = (Math.floor(Math.random() * (3 - 1) + 1));;
        }else{
            bet = 1;
        }
    }else{
        isTwoToFive = Math.floor(Math.random() * (100));
        if(isTwoToFive < 80){
            scale = (Math.floor(Math.random() * (5 - 3) + 3));
        }else{
            isFiveToTen = Math.floor(Math.random() * (100));
            if(isFiveToTen < 80){
                scale = (Math.floor(Math.random() * (10 - 5) + 5))
            }else{
                isTenToFifty = Math.floor(Math.random() * (100));
                if(isTenToFifty < 80){
                    scale = (Math.floor(Math.random() * (50 - 10) + 10))
                }else{
                    isFiftyToHundred = Math.floor(Math.random() * (100));
                    if(isFiftyToHundred < 80){
                        scale = (Math.floor(Math.random() * (100 - 50) + 50))
                    }else{
                        scale = (Math.floor(Math.random() * (2500 - 100) + 100))
                    }
                }
            }
        }
    }
    
    bet = bet * scale;
    bet = parseFloat(bet.toFixed(2));
    // bet = parseFloat(2871.40);
    console.log(bet);
    return bet;
}

/* 
   = recursive counter
   ---------------------------------------------------- */
function displayTimer(io, randomNumber, gameId) {
    var coin_timer = 1.00;
    var sendData = {
        finalValue: randomNumber,
        xValue: coin_timer,
        displayValue: '',
        type:1,
        gameId: gameId,
        randomUser:null
    };

    var timer = setInterval(function run() {
        let calculatedTime = sendData.xValue;

        
        if(parseFloat(randomNumber) === parseFloat(calculatedTime)) {
            pool.getConnection(function(err, connection) {
                if (err) throw err; // not connected!
                connection.query("Select * from user_bet_results where user_bet_game_id = " + gameId + ' and status = 2', function(err, rows) {
                    connection.release();
                    if(err) {
                        errorLog.error("error while fetching user bet result in recursive function", JSON.stringify(err));
                    } else {
                        if(rows.length) {
                            rows.map(row => {
                                updateUserInformations({userId: row.user_id}, row, false, true);
                            });
                        }
                    }
                });
            });
            pool.getConnection(function(err, connection) {
                if (err) throw err; // not connected!
                connection.query('UPDATE `user_bet_results` SET  ? WHERE user_bet_game_id = ' + gameId + ' and status = 2', { status: 0 }, function(err, result) {
                    if (err) {
                        errorLog.error("error while updating user bet result in recursive function", JSON.stringify(err));
                    } else {
                        sendData.displayValue = '';
                        sendData.type = 2;
                        sendData.xValue = '';
                        io.sockets.emit("crashStart", sendData);
                        clearInterval(timer);
                        (async () => {
                            await sleep(2000).then(() => {
                                let passRandomNumber = generateRandomNumber();
                                let gameObject = {
                                    name: "game at " +Date.now(),
                                    length: passRandomNumber
                                };
                                connection.query('INSERT INTO user_bet_games SET ?', gameObject, function(err, result) {
                                    connection.release();
                                    if (err) {
                                        errorLog.error("error while adding new user bet game after timeout in recursive function", JSON.stringify(err));
                                    } else {
                                        sendData.gameId = result.insertId;
                                        waitingTimerFunc(io, sendData, 6, passRandomNumber);
                                    }
                                }); 
                            })
                        })()
                    }
                });
            });
        } else {
            pool.getConnection(function(err, connection) {
                if (err) throw err; // not connected!
                connection.query("Select u.id, u.name, u.email, ubr.status, ubr.bet_at, ubr.y_axis, ubr.bet_amount, ubr.calculate_amount, ubr.calculate_xp, ubr.created_at from user_bet_results as ubr, users as u where ubr.user_id = u.id and ubr.status = 1 and ubr.user_bet_game_id = "+gameId+" order by ubr.bet_at", function(err, result) {
                    connection.release();
                    if(err) {
                        errorLog.error("error while fetching other user's game status in recursive function", JSON.stringify(err));
                    } else {
                        sendData.randomUser = result;
                        if(result.length) {
                            sendData.lastUser = result[result.length - 1];
                        } else {
                            sendData.lastUser = {};
                        }
                        sendData.displayValue = '';
                        sendData.type = 1;
                        sendData.xValue = (parseFloat(sendData.xValue) + 0.01).toFixed(2);
                        io.sockets.emit("crashStart", sendData);
                        if((parseInt(sendData.xValue) > 1) && (parseInt(sendData.xValue) < 5)) {
                            clearInterval(timer);
                            timer = setInterval(run, 75);
                        } else if((parseInt(sendData.xValue) > 4) && (parseInt(sendData.xValue) < 10)) {
                            clearInterval(timer);
                            timer = setInterval(run, 50);
                        } else if(parseInt(sendData.xValue) > 9) {
                            clearInterval(timer);
                            timer = setInterval(run, 25);
                        }
                    }
                });
            });
        }
    }, 100);
}

/* 
   = waiting time for number of second (passed in parameter) 
   = before next game starts
   ---------------------------------------------------- */
function waitingTimerFunc(io, sendData, waitingTimerCount, randomNumber) {
    var waitingTimer = setInterval(function() {
        if (waitingTimerCount > 0) {
            waitingTimerCount--;
            pool.getConnection(function(err, connection) {
                if (err) throw err; // not connected!
                connection.query("Select u.id, u.name, u.email, ubr.status, ubr.bet_at, ubr.y_axis, ubr.bet_amount, ubr.calculate_amount, ubr.calculate_xp, ubr.created_at from user_bet_results as ubr, users as u where ubr.user_id = u.id and ubr.status = 2 and ubr.user_bet_game_id = "+sendData.gameId+" order by ubr.id", function(err, result) {
                        connection.release();
                        if(err) {
                            errorLog.error("error while fetching user bet result in waiting timer function", JSON.stringify(err));
                        } else {
                            sendData.randomUser = result;
                            if(result.length) {
                                sendData.lastUser = result[result.length - 1];
                            } else {
                                sendData.lastUser = {};
                            }
                            sendData.displayValue = waitingTimerCount;
                            sendData.xValue = '';
                            sendData.type = 3;
                            io.sockets.emit("crashStart", sendData);
                        }
                });
            });
        } else {
            clearInterval(waitingTimer);
            displayTimer(io, randomNumber, sendData.gameId);
        }
    },1000);
}

/* 
   = send push notification  
   ---------------------------------------------------- */
function sendPushNotification(pushdata) {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
        connection.query("Select device_token from users where is_active = 1 and is_block = 0 and social_media_type != 0 and rankingByLevel > 1 and id =" + pushdata.pushId, function(err, toresult) {
            if(err) {
                errorLog.error("error while sending push notification -> fetching user details.", JSON.stringify(err));
            } else {
                if(toresult.length) {
                    let dviceToken = toresult[0].device_token;
                    connection.query("Select id, name, avatar, device_token from users where is_active = 1 and is_block = 0 and social_media_type != 0 and rankingByLevel > 1 and id =" + pushdata.userId, function(err, result) {
                        connection.release();
                        if(err) {
                            errorLog.error("error while sending push notification -> fetching user details.", JSON.stringify(err));
                        } else {
                            errorLog.info("query responded");
                            if(result.length) {
                                var user = result[0];
                                // errorLog.info("user", user);
                                var payload = {
                                    data: {
                                        imageurl: user.avatar,
                                        isBrodcast: (pushdata.messageType === 2) ? "1" : "0",
                                        type: pushdata.messageType.toString(),
                                        sound: "default",
                                        title: user.name + " sends message",
                                        message: pushdata.message,
                                        id: user.id.toString()
                                    },
                                    token: dviceToken
                                };
                                errorLog.info("payload " + JSON.stringify(payload));
                                var options = {
                                    priority: "high",
                                    timeToLive: 60 * 60 *24
                                };
                                admin.messaging().send(payload)
                                .then(function(response) {
                                    errorLog.info("Successfully sent push notification", JSON.stringify(response));
                                })
                                .catch(function(error) {
                                    errorLog.error("error while sending push notification", JSON.stringify(error));
                                });
                            };
                        }
                    });
                }
            }
        });
    });



}